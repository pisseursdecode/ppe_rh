﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroFramework.Forms;
using MetroFramework;
using PPE_Rh_V2;

namespace PPE_Rh_V2
{
    public partial class CentreDeControle : MetroForm
    {
        public CentreDeControle()
        {
            InitializeComponent();
        }


        private void metroToolTip1_Popup(object sender, System.Windows.Forms.PopupEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {

            loadComboBox();
           listeDesCollaborateurs1.Visible = false;
            listeRegonSecteur1.Visible = false;
            resumerDeLaBase1.Visible = true;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           listeDesCollaborateurs1.Visible = true;
           resumerDeLaBase1.Visible = false;
            listeRegonSecteur1.Visible = false;
        }

        private void loadComboBox()
        {
            //selection d'un collaborateur
            comboBox1.Items.Add("Visiteurs");
            comboBox1.Items.Add("Prescipteur");
            comboBox1.Items.Add("Responsable");
            comboBox1.Items.Add("Délégué");
            comboBox1.Items.Add("Tout");

            // Modification d'un collaborateur
            comboBox2.Items.Add("Visiteurs");
            comboBox2.Items.Add("Prescipteur");
            comboBox2.Items.Add("Responsable");
            comboBox2.Items.Add("Délégué");


            comboBox3.Items.Add("Région");
            comboBox3.Items.Add("Secteur");

        }

        private void button9_Click(object sender, EventArgs e)
        {
            listeDesCollaborateurs1.Visible = false;
            resumerDeLaBase1.Visible = false;
            listeRegonSecteur1.Visible = true;
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            
            if (comboBox2.Text == "Visiteurs")
            {
                Api.Instance.tempFile.titreAjoutPersonne = "Ajout d'un Visiteurs : ";
                Api.Instance.tempFile.tagAjout = 0; 
                Ajout_Personne Winform = new Ajout_Personne(this);
                Winform.Show();
            }
            else if (comboBox2.Text == "Prescipteur")
            {
                Api.Instance.tempFile.titreAjoutPersonne = "Ajout d'un Prescipteur : ";
                Api.Instance.tempFile.tagAjout = 1;
                Ajout_Personne Winform = new Ajout_Personne(this);
                Winform.Show();
            }
            else if (comboBox2.Text == "Responsable")
            {
                Api.Instance.tempFile.titreAjoutPersonne = "Ajout d'un Responsable : ";
                Api.Instance.tempFile.tagAjout = 2;
                Ajout_Personne Winform = new Ajout_Personne(this);
                Winform.Show();
            }
            else if (comboBox2.Text == "Délégué")
            {
                Api.Instance.tempFile.titreAjoutPersonne = "Ajout d'un Délégué : ";
                Api.Instance.tempFile.tagAjout = 3;
                Ajout_Personne Winform = new Ajout_Personne(this);
                Winform.Show();
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "Secteur")
            {
                Ajout_Secteur winform = new Ajout_Secteur(this);
                winform.Show();
            }
            else
            {
                Ajout_Région winform = new Ajout_Région(this);
                winform.Show();
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"C:\Users\Stephane Mouawad\Documents\Visual Studio 2015\Projects\PPE_Rh_V2\fff.pdf");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Planning winform = new Planning();
            winform.Show();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Notes_des_visites winform = new Notes_des_visites();
            winform.Show();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            Valider_frais winform = new Valider_frais(this);
            winform.Show();
        }
    }
}
