﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPE_Rh_V2
{
    public class Secteur
    {
        int id;
        string nom;
        int id_responsable;

     

        public Secteur(string nom, int id_responsable)
        {
            this.nom = nom;
            this.Id_responsable = id_responsable;
        }

        public Secteur()
        {

        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }


        public int Id_responsable
        {
            get
            {
                return id_responsable;
            }

            set
            {
                id_responsable = value;
            }
        }
    }
}
