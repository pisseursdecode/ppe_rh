﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE_Rh_V2
{
    public partial class ListeDesCollaborateurs : UserControl
    {
        public ListeDesCollaborateurs()
        {
            InitializeComponent();
        }

        private void ListeDesCollaborateurs_Load(object sender, EventArgs e)
        {
            Api.Instance.interrogationServer.getRegion();
            Api.Instance.interrogationServer.getSecteur();

            foreach (Region item in Api.Instance.tempFile.arrayRegion)
            {
                comboBox1.Items.Add(item.Nom);
            }

            foreach (Secteur item in Api.Instance.tempFile.arraySecteur)
            {
                comboBox2.Items.Add(item.Nom);
            }

            foreach (Personne item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Type == ConsultationPersonne.visiteurs)
                {
                    metroGrid1.Rows.Add(item.Id, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.Ville, item.CodePostal, item.Telephone, item.Email);
                }
                else if (item.Type == ConsultationPersonne.deleges)
                {
                    metroGrid1.Rows.Add(item.Id, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.Ville, item.CodePostal, item.Telephone, item.Email);
                }
                else if (item.Type == ConsultationPersonne.responsables)
                {
                    metroGrid1.Rows.Add(item.Id, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.Ville, item.CodePostal, item.Telephone, item.Email);
                }
                else if (item.Type == ConsultationPersonne.prescripteurs)
                {
                    metroGrid1.Rows.Add(item.Id, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.Ville, item.CodePostal, item.Telephone, item.Email);
                }
            }
        }
    }
}
