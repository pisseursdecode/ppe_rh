﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using PPE_Rh_V2;

namespace PPE_Rh_V2
{
    public partial class Ajout_Région : MetroForm
    {
       CentreDeControle centre = null;

        public Ajout_Région(CentreDeControle centrePost)
        {
            InitializeComponent();
            centre = centrePost;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {

            foreach (var item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Type == ConsultationPersonne.responsables)
                {
                    metroComboBox1.Items.Add(item);
                }
            }

            Region uneRegion = new Region(metroTextBox1.Text, (metroComboBox1.SelectedItem as ComboboxItem).ToValue(), (metroComboBox2.SelectedItem as ComboboxItem).ToValue());
            Api.Instance.interrogationServer.gestionRegions(ActionRequete.ajouter, uneRegion);
           // centre.loadData();
            this.Close();
        }

        private void Ajout_Région_Load(object sender, EventArgs e)
        {
            // on liste les responsables

            Api.Instance.interrogationServer.getPersonne();
            foreach (var personne in Api.Instance.tempFile.arrayPersonnes)
            {
                if (personne.Type == ConsultationPersonne.responsables)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = personne.Nom + " " + personne.Prenom;
                    item.Value = personne.Id;

                    metroComboBox1.Items.Add(item);
                }
            }

            //on liste les secteurs

            Api.Instance.interrogationServer.getSecteur();
            foreach (var secteur in Api.Instance.tempFile.arraySecteur)
            {
                ComboboxItem item = new ComboboxItem();
                item.Text = secteur.Nom;
                item.Value = secteur.Id;
                metroComboBox2.Items.Add(item);
            }
        }
    }
}
