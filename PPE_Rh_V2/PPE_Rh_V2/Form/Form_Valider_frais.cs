﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using PPE_Rh_V2;

namespace PPE_Rh_V2
{
    public partial class Valider_frais : MetroForm
    {
        CentreDeControle centre = null;

        public Valider_frais(CentreDeControle centrePost)
        {
            InitializeComponent();
            centre = centrePost;
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Valider_frais_Load(object sender, EventArgs e)
        {
            Planning monpla = new Planning();
            monpla.loadData();

            Api.Instance.interrogationServer.getVisites();
            Api.Instance.interrogationServer.getPersonne();
            Api.Instance.interrogationServer.getFrais();
            this.Text = "Les frais à valider";
            
            dataGridView1.Rows.Clear();

            var listeDesFrais = Api.Instance.tempFile.arrayFrais;
            var listeDesVisites = Api.Instance.tempFile.arrayVisite;
            var listeDesCPersonne = Api.Instance.tempFile.arrayPersonnes;

            for (int itemFrais = 0; itemFrais < listeDesFrais.Count; itemFrais ++)
            {
                for (int itemPersonne = 0; itemPersonne < listeDesCPersonne.Count; itemPersonne++)
                {
                    for (int itemVisite = 0; itemVisite < listeDesVisites.Count; itemVisite++)
                    {
                        if (listeDesFrais[itemFrais].Fk_visite_id == listeDesVisites[itemVisite].Id && listeDesVisites[itemVisite].Fk_id_collaborateur == listeDesCPersonne[itemPersonne].Id)
                        {
                            dataGridView1.Rows.Add(listeDesVisites[itemVisite].DebutVisite, listeDesFrais[itemFrais].Type, listeDesFrais[itemFrais].Tarif, listeDesVisites[itemVisite].CodePostal, listeDesVisites[itemVisite].Ville, listeDesCPersonne[itemPersonne].Nom, listeDesCPersonne[itemPersonne].Prenom, listeDesFrais[itemFrais].Id, listeDesCPersonne[itemPersonne].Id);              
                        }
                    }
                }         
            }
            dataGridView1.AllowUserToAddRows = false;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {

                if (row.Selected == true)
                {
                    FraisDeRoute unFais = new FraisDeRoute();
                    unFais.Id = (Int32)row.Cells[7].Value;
                    unFais.Fk_visite_id = (int)row.Cells[8].Value;
                    unFais.Status = 1;
                    unFais.Type = (String)row.Cells[1].Value;
                    unFais.Tarif = (float)row.Cells[2].Value;
                    Api.Instance.interrogationServer.gestionFraisDeRoute(ActionRequete.modifier, unFais);
                }
                
            }
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {

                if (row.Selected == true)
                {
                    FraisDeRoute unFais = new FraisDeRoute();
                    unFais.Id = (Int32)row.Cells[7].Value;
                    unFais.Fk_visite_id = (int)row.Cells[8].Value;
                    unFais.Status = 0;
                    unFais.Type = (String)row.Cells[1].Value;
                    unFais.Tarif = (float)row.Cells[2].Value;
                    Api.Instance.interrogationServer.gestionFraisDeRoute(ActionRequete.modifier, unFais);
                }

            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                if (row.Selected == true)
                {
                    FraisDeRoute unFais = new FraisDeRoute();
                    unFais.Id = (Int32)row.Cells[7].Value;
                    unFais.Fk_visite_id = (int)row.Cells[8].Value;
                    unFais.Type = (String)row.Cells[1].Value;
                    unFais.Tarif = (float)row.Cells[2].Value;
                    Api.Instance.interrogationServer.gestionFraisDeRoute(ActionRequete.modifier, unFais);
                    MessageBox.Show("Les modifications ont été apportées");
                }

            }
        }
    }
}
