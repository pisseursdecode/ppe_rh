﻿using System;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{
    public partial class Connect : MetroForm
    {
        public Connect()
        {
            InitializeComponent();
        }

        private void Connect_Load(object sender, EventArgs e)
        {
            metroTextBox2.KeyDown += new KeyEventHandler(tb_KeyDown); 

        }


        private void tb_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                login();
            }
        }

        public void login()
        {
            if (Api.Instance.interrogationServer.SeConnecter(metroTextBox1.Text, metroTextBox2.Text) == "OK")
            {
                switch (Api.Instance.tempFile.typePersonne)
                {
                    case 0:
                        Planning_visiteur Planning_Visiteur = new Planning_visiteur();
                        Planning_Visiteur.Show();
                        break;
                    case 1:
                        Console.WriteLine("Case 2");
                        break;
                    case 2:
                        CentreDeControle Centre_Controle = new CentreDeControle();
                        Centre_Controle.Show();
                        break;
                    case 3:
                        Console.WriteLine("Case 2");
                        break;
                }
            }
            else if (Api.Instance.interrogationServer.SeConnecter(metroTextBox1.Text, metroTextBox2.Text) == "faillog")
            {
                MessageBox.Show("Mauvais login ou mot de passe", "La connexion a échoué", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void metroButton1_Click_1(object sender, EventArgs e)
        {
            login();
        }
    }
}
