﻿namespace PPE_Rh_V2
{
    partial class Notes_des_visites
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.Name2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Adress2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comp_adress2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CP2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.City2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Tel2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Email2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Name_visiteur2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Surname_visit2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.metroDateTime1 = new MetroFramework.Controls.MetroDateTime();
            this.metroDateTime2 = new MetroFramework.Controls.MetroDateTime();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.BackColor = System.Drawing.SystemColors.HotTrack;
            this.monthCalendar1.Location = new System.Drawing.Point(18, 126);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 0;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Name2,
            this.Adress2,
            this.Comp_adress2,
            this.CP2,
            this.City2,
            this.Tel2,
            this.Email2,
            this.Name_visiteur2,
            this.Surname_visit2});
            this.dataGridView1.Location = new System.Drawing.Point(257, 57);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1281, 345);
            this.dataGridView1.TabIndex = 1;
            // 
            // Name2
            // 
            this.Name2.HeaderText = "Nom";
            this.Name2.Name = "Name2";
            this.Name2.Width = 125;
            // 
            // Adress2
            // 
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.MediumTurquoise;
            this.Adress2.DefaultCellStyle = dataGridViewCellStyle5;
            this.Adress2.HeaderText = "Adresse";
            this.Adress2.Name = "Adress2";
            this.Adress2.Width = 150;
            // 
            // Comp_adress2
            // 
            this.Comp_adress2.HeaderText = "Complément d\'adresse";
            this.Comp_adress2.Name = "Comp_adress2";
            this.Comp_adress2.Width = 200;
            // 
            // CP2
            // 
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.MediumTurquoise;
            this.CP2.DefaultCellStyle = dataGridViewCellStyle6;
            this.CP2.HeaderText = "Code Postal";
            this.CP2.Name = "CP2";
            // 
            // City2
            // 
            this.City2.HeaderText = "Ville";
            this.City2.Name = "City2";
            this.City2.Width = 107;
            // 
            // Tel2
            // 
            dataGridViewCellStyle7.BackColor = System.Drawing.Color.MediumTurquoise;
            this.Tel2.DefaultCellStyle = dataGridViewCellStyle7;
            this.Tel2.HeaderText = "Téléphone";
            this.Tel2.Name = "Tel2";
            // 
            // Email2
            // 
            this.Email2.HeaderText = "Email";
            this.Email2.Name = "Email2";
            this.Email2.Width = 150;
            // 
            // Name_visiteur2
            // 
            dataGridViewCellStyle8.BackColor = System.Drawing.Color.MediumTurquoise;
            this.Name_visiteur2.DefaultCellStyle = dataGridViewCellStyle8;
            this.Name_visiteur2.HeaderText = "Nom du Visiteur";
            this.Name_visiteur2.Name = "Name_visiteur2";
            this.Name_visiteur2.Width = 150;
            // 
            // Surname_visit2
            // 
            this.Surname_visit2.HeaderText = "Prénom du Visiteur";
            this.Surname_visit2.Name = "Surname_visit2";
            this.Surname_visit2.Width = 150;
            // 
            // metroDateTime1
            // 
            this.metroDateTime1.Location = new System.Drawing.Point(9, 341);
            this.metroDateTime1.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime1.Name = "metroDateTime1";
            this.metroDateTime1.Size = new System.Drawing.Size(200, 29);
            this.metroDateTime1.TabIndex = 8;
            // 
            // metroDateTime2
            // 
            this.metroDateTime2.Location = new System.Drawing.Point(9, 431);
            this.metroDateTime2.MinimumSize = new System.Drawing.Size(0, 29);
            this.metroDateTime2.Name = "metroDateTime2";
            this.metroDateTime2.Size = new System.Drawing.Size(200, 29);
            this.metroDateTime2.TabIndex = 9;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(9, 309);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(108, 19);
            this.metroLabel1.TabIndex = 10;
            this.metroLabel1.Text = "Heure de début :";
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.Location = new System.Drawing.Point(9, 394);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(88, 19);
            this.metroLabel2.TabIndex = 11;
            this.metroLabel2.Text = "Heure de fin :";
            // 
            // Notes_des_visites
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1531, 483);
            this.Controls.Add(this.metroLabel2);
            this.Controls.Add(this.metroLabel1);
            this.Controls.Add(this.metroDateTime2);
            this.Controls.Add(this.metroDateTime1);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.monthCalendar1);
            this.Name = "Notes_des_visites";
            this.Text = "Notes_des_visites";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Adress2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comp_adress2;
        private System.Windows.Forms.DataGridViewTextBoxColumn CP2;
        private System.Windows.Forms.DataGridViewTextBoxColumn City2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Tel2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Email2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Name_visiteur2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Surname_visit2;
        private MetroFramework.Controls.MetroDateTime metroDateTime1;
        private MetroFramework.Controls.MetroDateTime metroDateTime2;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
    }
}