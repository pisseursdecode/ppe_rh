﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPE_Rh_V2
{
    public class Personne
    {
        int id;
        string nom;
        string prenom;
        string adresse;
        string complemment; // option
        string ville;
        int codePostal;
        int telephone;
        string email;
        string login;
        string mdp;
        int type;

        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }

        public string Prenom
        {
            get
            {
                return prenom;
            }

            set
            {
                prenom = value;
            }
        }

        public string Adresse
        {
            get
            {
                return adresse;
            }

            set
            {
                adresse = value;
            }
        }

        public string Complemment
        {
            get
            {
                return complemment;
            }

            set
            {
                complemment = value;
            }
        }

        public string Ville
        {
            get
            {
                return ville;
            }

            set
            {
                ville = value;
            }
        }

        public int CodePostal
        {
            get
            {
                return codePostal;
            }

            set
            {
                codePostal = value;
            }
        }

        public int Telephone
        {
            get
            {
                return telephone;
            }

            set
            {
                telephone = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string Login
        {
            get
            {
                return login;
            }

            set
            {
                login = value;
            }
        }

        public string Mdp
        {
            get
            {
                return mdp;
            }

            set
            {
                mdp = value;
            }
        }

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }


        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }


        public Personne(string nom, string prenom, string adresse, string complemment, string ville, int codePostal, int telephone, string email, string login, string mdp, int type) {
            this.nom = nom;
            this.prenom = prenom;
            this.adresse = adresse;
            this.complemment = complemment;
            this.ville = ville;
            this.codePostal = codePostal;
            this.telephone = telephone;
            this.email = email;
            this.login = login;
            this.mdp = mdp;
            this.type = type;
        }

      public Personne(){

        }

    }
}
