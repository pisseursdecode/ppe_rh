﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{
    public partial class Ajout_Note : MetroForm
    {
        public Ajout_Note()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Notes note = new Notes(metroTextBox1.Text, richTextBox1.Text);
            Api.Instance.interrogationServer.gestionDesNote(ActionRequete.ajouter, note);
        }
    }
}
