﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{

    public partial class Ajout_visite : MetroForm
    {
        Planning Planning = null;
        public Ajout_visite(Planning planningPost)
        {
            InitializeComponent();
            Planning = planningPost;
        }
        DateTime date3 = DateTime.Today;


        private void metroDateTime1_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime1.Format = DateTimePickerFormat.Time;
            this.metroDateTime1.Width = 100;
            this.metroDateTime1.ShowUpDown = true;
        }
        
        private void metroDateTime2_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime2.Format = DateTimePickerFormat.Time;
            this.metroDateTime2.Width = 100;
            this.metroDateTime2.ShowUpDown = true;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            Visite visite = new Visite(metroDateTime4.Text + " " + metroDateTime1.Text, metroTextBox2.Text, metroTextBox3.Text, metroTextBox4.Text, metroTextBox5.Text, Convert.ToInt32(metroTextBox6.Text), metroTextBox7.Text, Convert.ToInt32(metroTextBox8.Text), metroTextBox9.Text, metroDateTime3.Text+ " " + metroDateTime2.Text);
            Console.WriteLine(visite);
            Api.Instance.interrogationServer.gestionVisites(ActionRequete.ajouter, visite);
            Planning.loadData();
            this.Close();

        }

        private void Ajout_visite_Load(object sender, EventArgs e)
        {

        }
    }
}
