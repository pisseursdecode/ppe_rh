﻿
namespace PPE_Rh_V2
{
    public class FraisDeRoute
    {
        int id;
        string type;
        float tarif;
        int status;
        int fk_visite_id;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string  Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public float Tarif
        {
            get
            {
                return tarif;
            }

            set
            {
                tarif = value;
            }
        }

        public int Status
        {
            get
            {
                return status;
            }

            set
            {
                status = value;
            }
        }

        public int Fk_visite_id
        {
            get
            {
                return fk_visite_id;
            }

            set
            {
                fk_visite_id = value;
            }
        }

        public FraisDeRoute(int id, string type, float tarif, int status, int fk_visite_id)
        {
            this.id = id;
            this.type = type;
            this.tarif = tarif;
            this.status = status;
            this.fk_visite_id = fk_visite_id;
        }

        public FraisDeRoute()
        {

        }
    }
}
