﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE_Rh_V2
{
    public partial class ResumerDeLaBase : UserControl
    {
        public ResumerDeLaBase()
        {
            InitializeComponent();
        }

        private void ResumerDeLaBase_Load(object sender, EventArgs e)
        {

            Api.Instance.interrogationServer.getPersonne();
            Api.Instance.interrogationServer.getRegion();
            Api.Instance.interrogationServer.getSecteur();
            int[] dataChart1 = { 0, 0, 0, 0 };
            int[] dataChart2 = { 5, 2, 6, 8 };
            

            foreach (var item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Type == 0)
                {
                    dataChart1[0] += 1;
                }
                else if (item.Type == 1)
                {
                    dataChart1[1] += 1;
                }
                else if (item.Type == 2)
                {
                    dataChart1[2] += 1;
                }
                else if (item.Type == 3)
                {
                    dataChart1[3] += 1;
                }
            }


            
                for (int i = 0; i < Api.Instance.tempFile.arraySecteur.Count; i++)
                {
                 foreach (var item in Api.Instance.tempFile.arrayRegion)
                {
                }
            }



            // chart  1 

            string[] xValues = { "Visiteur", "Responsable", "Délégué","Prescripteurs" };
            chart1.Series["Series1"].Points.DataBindXY(xValues, dataChart1);

            // pour rajouter des couleurs

            chart1.Series["Series1"].Points[0].Color = Color.MediumSeaGreen;
            chart1.Series["Series1"].Points[1].Color = Color.PaleGreen;
            chart1.Series["Series1"].Points[2].Color = Color.LawnGreen;

            chart1.Series["Series1"]["PieLabelStyle"] = "Disabled";

            chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

            chart1.Legends[0].Enabled = true;

            ///////////////// 


            // chart  2 

            // faire des requette pour celle la 
            // recuper le nombre de secteur et le nombre de region en fonction de chaque secteur
            string[] xValues2 = { "Nord", "Est", "Ouest", "Sud" };
            chart2.Series["Series1"].Points.DataBindXY(xValues2, dataChart2);

            chart2.Series["Series1"]["PieLabelStyle"] = "Disabled";

            chart2.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;

            chart2.Legends[0].Enabled = true;
        }
    }
}
