﻿namespace PPE_Rh_V2
{
    partial class Valider_frais
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.metroButton1 = new MetroFramework.Controls.MetroButton();
            this.metroButton2 = new MetroFramework.Controls.MetroButton();
            this.metroButton3 = new MetroFramework.Controls.MetroButton();
            this.Date_Visite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Type_Frais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Coût = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CP_Visite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Ville_visite = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nom_visiteur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Prenom_Visiteur = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdFrais = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.idColl = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Date_Visite,
            this.Type_Frais,
            this.Coût,
            this.CP_Visite,
            this.Ville_visite,
            this.Nom_visiteur,
            this.Prenom_Visiteur,
            this.IdFrais,
            this.idColl});
            this.dataGridView1.Location = new System.Drawing.Point(0, 63);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(1292, 373);
            this.dataGridView1.TabIndex = 2;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // metroButton1
            // 
            this.metroButton1.Location = new System.Drawing.Point(651, 441);
            this.metroButton1.Name = "metroButton1";
            this.metroButton1.Size = new System.Drawing.Size(97, 36);
            this.metroButton1.TabIndex = 3;
            this.metroButton1.Text = "Valider";
            this.metroButton1.UseSelectable = true;
            this.metroButton1.Click += new System.EventHandler(this.metroButton1_Click);
            // 
            // metroButton2
            // 
            this.metroButton2.Location = new System.Drawing.Point(811, 441);
            this.metroButton2.Name = "metroButton2";
            this.metroButton2.Size = new System.Drawing.Size(92, 36);
            this.metroButton2.TabIndex = 4;
            this.metroButton2.Text = "Modifier";
            this.metroButton2.UseSelectable = true;
            this.metroButton2.Click += new System.EventHandler(this.metroButton2_Click);
            // 
            // metroButton3
            // 
            this.metroButton3.Location = new System.Drawing.Point(968, 441);
            this.metroButton3.Name = "metroButton3";
            this.metroButton3.Size = new System.Drawing.Size(93, 36);
            this.metroButton3.TabIndex = 5;
            this.metroButton3.Text = "Rejeter";
            this.metroButton3.UseSelectable = true;
            this.metroButton3.Click += new System.EventHandler(this.metroButton3_Click);
            // 
            // Date_Visite
            // 
            this.Date_Visite.HeaderText = "Date de Visite";
            this.Date_Visite.Name = "Date_Visite";
            this.Date_Visite.Width = 150;
            // 
            // Type_Frais
            // 
            this.Type_Frais.HeaderText = "Type de Frais";
            this.Type_Frais.Name = "Type_Frais";
            this.Type_Frais.Width = 150;
            // 
            // Coût
            // 
            this.Coût.HeaderText = "Coût";
            this.Coût.Name = "Coût";
            // 
            // CP_Visite
            // 
            this.CP_Visite.HeaderText = "Code Postale de la Visite";
            this.CP_Visite.Name = "CP_Visite";
            // 
            // Ville_visite
            // 
            this.Ville_visite.HeaderText = "Ville de la Visite";
            this.Ville_visite.Name = "Ville_visite";
            this.Ville_visite.Width = 150;
            // 
            // Nom_visiteur
            // 
            this.Nom_visiteur.HeaderText = "Nom du Visiteur";
            this.Nom_visiteur.Name = "Nom_visiteur";
            this.Nom_visiteur.Width = 150;
            // 
            // Prenom_Visiteur
            // 
            this.Prenom_Visiteur.HeaderText = "Prénom du Visiteur";
            this.Prenom_Visiteur.Name = "Prenom_Visiteur";
            this.Prenom_Visiteur.Width = 449;
            // 
            // IdFrais
            // 
            this.IdFrais.HeaderText = "Column1";
            this.IdFrais.Name = "IdFrais";
            this.IdFrais.Visible = false;
            // 
            // idColl
            // 
            this.idColl.HeaderText = "Column1";
            this.idColl.Name = "idColl";
            this.idColl.Visible = false;
            // 
            // Valider_frais
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1294, 487);
            this.Controls.Add(this.metroButton3);
            this.Controls.Add(this.metroButton2);
            this.Controls.Add(this.metroButton1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "Valider_frais";
            this.Text = "0.";
            this.Load += new System.EventHandler(this.Valider_frais_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.DataGridView dataGridView1;
        private MetroFramework.Controls.MetroButton metroButton1;
        private MetroFramework.Controls.MetroButton metroButton2;
        private MetroFramework.Controls.MetroButton metroButton3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date_Visite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Type_Frais;
        private System.Windows.Forms.DataGridViewTextBoxColumn Coût;
        private System.Windows.Forms.DataGridViewTextBoxColumn CP_Visite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Ville_visite;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nom_visiteur;
        private System.Windows.Forms.DataGridViewTextBoxColumn Prenom_Visiteur;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdFrais;
        private System.Windows.Forms.DataGridViewTextBoxColumn idColl;
    }
}