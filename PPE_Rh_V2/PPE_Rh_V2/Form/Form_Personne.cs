﻿using System;
using MetroFramework.Forms;
using PPE_Rh_V2;

namespace PPE_Rh_V2
{
    public partial class Ajout_Personne : MetroForm
    {
        CentreDeControle centre = null;

        public Ajout_Personne(CentreDeControle centrePost)
        {
            InitializeComponent();
            centre = centrePost;
        }

        private void metroTextBox1_Click(object sender, EventArgs e)
        {

        }

        private void Ajout_Personne_Load(object sender, EventArgs e)
        {
            Text = Api.Instance.changerLeTitre.recupererLeTitre();

        }

        private void button1_Click(object sender, EventArgs e)
        {



            Personne unePersonne = new Personne(metroTextBox1.Text, metroTextBox2.Text, metroTextBox3.Text, metroTextBox4.Text, metroTextBox5.Text, Convert.ToInt32(metroTextBox6.Text), Convert.ToInt32(metroTextBox7.Text), metroTextBox8.Text, metroTextBox9.Text, metroTextBox10.Text, Api.Instance.tempFile.tagAjout);
            Console.WriteLine(unePersonne);
            Api.Instance.interrogationServer.gestionCollaborateur(ActionRequete.ajouter, unePersonne);
           // centre.loadData();
            this.Close();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            metroTextBox10.Text = Api.Instance.methodeUtile.mdpAleatoir(9);
        }

        
    }
}
