﻿using System;
using System.Collections.Generic;
using MySql.Data.MySqlClient;
using System.Data;
using System.Windows.Forms;
using MetroFramework;

namespace PPE_Rh_V2
{

    public class InterrogationServer
    {
        private static string maChaineconnexion = "SERVER=89.38.148.48; DATABASE=ppe_rh; UID=rootppe; PASSWORD=rootppe";
        private static MySqlCommand maCommande = new MySqlCommand();
        // le constructeur 
        public InterrogationServer()
        {

        }


        // Un utilisateur peut etre un Responsable, Délégué ou un Visiteur
        // 0 = visiteur / 1 = prescripteur / 2 = responsable / 3 = délegué





        /*-------------------------------------------  Authentification  --------------------------------------------------------------------*/


        // SeConnecter
        public string SeConnecter(string identifiant, string motDePasse)
        {
            string result = "failco"; // correspond à l'etat de connexion ("true" pour ok "false" pour fail)
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            int type = -1;

            try
            {
                maConnexion.Open();
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


            if (maConnexion.State.ToString() == "Open")
            {
                result = "OK";
                maCommande.Connection = maConnexion;

                maCommande.CommandType = CommandType.Text;

                // on récupere le mot de passe de cet utilisateur grace à la requette

                maCommande.CommandText = "SELECT col_Type FROM collaborateurs WHERE col_Login ='" + identifiant + "' AND col_Mdp ='" + motDePasse + "'";
                MySqlDataReader monReader = maCommande.ExecuteReader();

                while (monReader.Read())
                {
                    type = Convert.ToInt32(monReader.GetString(0));
                }
                if (type != -1)
                {
                    Api.Instance.tempFile.passwordUser = motDePasse;
                    Api.Instance.tempFile.idUser = identifiant;
                    Api.Instance.tempFile.typePersonne = type;

                }
                else
                {
                    result = "faillog";
                }

                monReader.Close();
            }
            else
            {
                result = "failco";
            }
            maConnexion.Close();
            return result;
        }

        /*-------------------------------------------  Modification de la base de donnée  --------------------------------------------------------------------*/




        // Gestion des collaborateurs
        public void gestionCollaborateur(string tagRequete, Personne collaborateur)
        {
            var colaborateur = new Dictionary<string, string> {
            { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`collaborateurs` (`col_Nom`, `col_Prenom`, `col_Adresse`, `col_Complement`, `col_Ville`, `col_CP`, `col_Tel`, `col_Mail`, `col_Region`, `col_Secteur`) VALUES ('" + collaborateur.Nom + "', '" + collaborateur.Prenom + "', '" + collaborateur.Adresse + "', '" + collaborateur.Complemment + "', '" + collaborateur.Ville + "', '" + collaborateur.CodePostal + "', '" + collaborateur.Telephone + "', '" + collaborateur.Email + "')" },
            { ActionRequete.modifier, "UPDATE `ppe_rh`.`collaborateurs` SET `col_Nom` = '" + collaborateur.Nom + "', `col_Prenom` = '" + collaborateur.Prenom + "', `col_Adresse` = '" + collaborateur.Adresse + "', `col_Complement` = '" + collaborateur.Complemment + "', `col_Ville` = '" + collaborateur.Ville + "', `col_CP` = '" + collaborateur.CodePostal + "', `col_Tel` = '" + collaborateur.Telephone + "', `col_Mail` = '" + collaborateur.Email + "' WHERE col_id = "+collaborateur.Id },
            { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`collaborateurs` WHERE col_id = "+collaborateur.Id}
        };
            setModifications(colaborateur[tagRequete], false);
        }

        // Gestion des visites
        public void gestionVisites(string tagRequete, Visite visite)
        {
            var visites = new Dictionary<string, string> {
            { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`visites` (`visiteur_Nom`, `visiteur_Prenom`, `visite_adresse`, `visite_complement`, `visite_ville`, `visite_CP`, `visite_tel`, `visite_mail`, `visite_debut`, `visite_fin`, `fk_note_Id`, `fk_col_Id`) VALUES ('"+visite.Nom+"', '"+visite.Prenom+"', '"+visite.Adresse+"', '"+visite.Complemment+"', '"+visite.Ville+"', "+visite.CodePostal+", '"+visite.Telephone+"', '"+visite.Email+"', '"+visite.DebutVisite+"', '"+visite.FinVisite+"', "+visite.Fk_id_note+", "+visite.Fk_id_collaborateur+")" },
            { ActionRequete.modifier, "UPDATE `ppe_rh`.`visites` SET `visiteur_Nom` = '"+visite.Nom+"', `visiteur_Prenom` = '"+visite.Prenom+"', `visite_adresse` = '"+visite.Adresse+"', `visite_complement` = '"+visite.Complemment+"', `visite_ville` = '"+visite.Ville+"', `visite_CP` = "+visite.CodePostal+", `visite_tel` = "+visite.Telephone+", `visite_mail` = '"+visite.Email+"' WHERE `Visite_Id` = " + visite.Id },
            { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`visites` WHERE `visites`.`Visite_Id` = " + visite.Id }
        };
            setModifications(visites[tagRequete], false);
        }

        // Gestion des frais de route
        public void gestionFraisDeRoute(string tagRequete, FraisDeRoute LesfraisdeRoutes)
        {
            var fraisDeRoute = new Dictionary<string, string> {
            { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`frais` (`frais_Type`, `frais_Tarif`, `frais_Status`, `fk_visite_Id`) VALUES ("+LesfraisdeRoutes.Type+", "+LesfraisdeRoutes.Tarif+", "+LesfraisdeRoutes.Status+", "+LesfraisdeRoutes.Tarif+")" },
            { ActionRequete.modifier, "UPDATE `ppe_rh`.`frais` SET `frais_Type` = '"+ LesfraisdeRoutes.Type + "', `frais_Tarif` = '"+LesfraisdeRoutes.Tarif +"', `frais_Status` = '"+LesfraisdeRoutes.Status +"' WHERE `frais_Id` = " + LesfraisdeRoutes.Id},
            { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`frais` WHERE frais_Id = " + LesfraisdeRoutes.Id }
        };

            setModifications(fraisDeRoute[tagRequete], true);
        }

        // Gestion des zones géographiques
        public void gestionRegions(string tagRequete, Region uneRegion)
        {
            var gestionRegions = new Dictionary<string, string> {
            { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`region` (`region_nom`, `id_responsable`, `fk_secteur_id`) VALUES ('"+uneRegion.Nom+"', "+uneRegion.Id_responsable+", "+uneRegion.Id_secteur+");" },
            { ActionRequete.modifier, "UPDATE `ppe_rh`.`region` SET `region_nom` = '"+uneRegion.Nom+"' WHERE `region`.`region_id` = " + uneRegion.Id},
            { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`region` WHERE `region_id` = " + uneRegion.Id }
        };
            setModifications(gestionRegions[tagRequete], false);
        }

        // Gestion des zones Secteurs
        public void gestionSecteurs(string tagRequete, Secteur unSecteur)
        {
            var gestionSecteurs = new Dictionary<string, string> {
            { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`secteur` (`secteur_nom`, `id_responsable`) VALUES ('"+unSecteur.Nom+"', "+unSecteur.Id_responsable+")" },
            { ActionRequete.modifier, "UPDATE `ppe_rh`.`secteur` SET `secteur_nom` = '"+unSecteur.Nom+"' WHERE `secteur`.`secteur_id` = " + unSecteur.Id},
            { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`secteur` WHERE `secteur_id` = " + unSecteur.Id }
        };
            setModifications(gestionSecteurs[tagRequete], false);
        }



        // Gestion des zones géographiques
        public void gestionDesNote(string tagRequete, Notes note)
        {
            var retourDesNote = new Dictionary<string, string> {
                { ActionRequete.ajouter, "INSERT INTO `ppe_rh`.`note` (`note_Sujet`, `note_Message`) VALUES ('"+note.Sujet+"', '"+note.Message+"')" },
                { ActionRequete.modifier, "UPDATE `ppe_rh`.`note` SET `note_Sujet` = '"+note.Sujet+"', `note_Message` = '"+note.Message+"' WHERE note_Id = " + note.Id },
                { ActionRequete.supprimer, "DELETE FROM `ppe_rh`.`note` WHERE `note_Id` = " + note.Id }
            };
            setModifications(retourDesNote[tagRequete], false);
        }
        /*------------------------------------------- Gestion des consultations et des modifications ----------------------------------------------------------------------------*/


        // cette procedure sert principalement à modifier les tables 
        static private void setModifications(string laRequete, bool showMessage)
        {
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = laRequete;
                maCommande.ExecuteNonQuery();

                if (showMessage)
                {
                    MessageBox.Show("Les modifications ont bien été apporté ", "Message");
                }

                maConnexion.Close();
            }
            // On exécute la requete

            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème, La requête a échoué !", "La requête a échoué");
            }
        }


        // cette procedure sert principalement à Consulter les tables 
        public void getPersonne()
        {

            List<Personne> arrayPersonne = new List<Personne>();
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `collaborateurs`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {

                    Personne personne = new Personne();
                    personne.Id = dataReader.GetInt32(0);
                    personne.Nom = dataReader.GetString(1);
                    personne.Prenom = dataReader.GetString(2);
                    personne.Adresse = dataReader.GetString(3);
                    personne.Complemment = dataReader.GetString(4);
                    personne.Ville = dataReader.GetString(5);
                    personne.CodePostal = dataReader.GetInt32(6);
                    personne.Telephone = dataReader.GetInt32(7);
                    personne.Email = dataReader.GetString(8);
                    personne.Login = dataReader.GetString(9);
                    personne.Mdp = dataReader.GetString(10);
                    personne.Type = dataReader.GetInt32(11);
                    arrayPersonne.Add(personne);
                }
                dataReader.Close();
                Api.Instance.tempFile.arrayPersonnes = arrayPersonne;
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();

        }


        public void getVisites()
        {

            List<Visite> arrayVisite = new List<Visite>();
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `visites`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {

                    Visite uneVisite = new Visite();
                    uneVisite.Id = dataReader.GetInt32(0);
                    uneVisite.Nom = dataReader.GetString(1);
                    uneVisite.Prenom = dataReader.GetString(2);
                    uneVisite.Adresse = dataReader.GetString(3);
                    uneVisite.Complemment = dataReader.GetString(4);
                    uneVisite.Ville = dataReader.GetString(5);
                    uneVisite.CodePostal = dataReader.GetInt32(6);
                    uneVisite.Telephone = dataReader.GetInt32(7);
                    uneVisite.Email = dataReader.GetString(8);
                    uneVisite.DebutVisite = dataReader.GetString(9);
                    uneVisite.FinVisite = dataReader.GetString(10);
                    uneVisite.Fk_id_note = dataReader.GetInt32(11);
                    uneVisite.Fk_id_collaborateur = dataReader.GetInt32(12);
                    arrayVisite.Add(uneVisite);
                }
                dataReader.Close();
                Api.Instance.tempFile.arrayVisite = arrayVisite;
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();
        }
        /*
                static private void gestionDesConsultationsPlaning()
                {
                    MySqlDataReader dataReader = maCommande.ExecuteReader();
                    MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
                    List<Visite> arrayVisite = new List<Visite>();
                    try
                    {
                        maConnexion.Open();
                        maCommande.Connection = maConnexion;
                        maCommande.CommandType = CommandType.Text;
                        maCommande.CommandText = maCommande.CommandText = "";

                        while (dataReader.Read())
                        {
                            Visite visite = new Visite();
                            visite.Nom = Convert.ToString(dataReader["id"]);
                            visite.Prenom = Convert.ToString(dataReader["id"]);
                            visite.Adresse = Convert.ToString(dataReader["id"]);
                            visite.Complemment = Convert.ToString(dataReader["id"]);
                            visite.Ville = Convert.ToString(dataReader["id"]);
                            visite.CodePostal = Convert.ToInt32(dataReader["id"]);
                            visite.Telephone = Convert.ToInt32(dataReader["id"]);
                            visite.Email = Convert.ToString(dataReader["id"]);
                            visite.DebutVisite = Convert.ToString(dataReader["id"]);
                            visite.FinVisite = Convert.ToString(dataReader["id"]);
                            visite.Prescripteur = Convert.ToString(dataReader["id"]);
                            arrayVisite.Add(visite);
                        }
                    }
                    catch (Exception)
                    {
                        MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
                    }
                    dataReader.Close();
                    maConnexion.Close();
                    Api.Instance.tempFile.arrayVisite = arrayVisite;
                }
        */

        public void getSecteur()
        {
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            List<Secteur> arraySecteur = new List<Secteur>();
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `secteur`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {
                    Secteur secteur = new Secteur();
                    secteur.Id = dataReader.GetInt32(0);
                    secteur.Nom = dataReader.GetString(1);
                    secteur.Id_responsable = dataReader.GetInt32(2);
                    arraySecteur.Add(secteur);

                }
                dataReader.Close();
                Api.Instance.tempFile.arraySecteur = arraySecteur;
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();

        }


        public void getRegion()
        {

            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            List<Region> listeDeRegion = new List<Region>();
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `region`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {
                    Region region = new Region();
                    region.Id = dataReader.GetInt32(0);
                    region.Nom = dataReader.GetString(1);
                    region.Id_responsable = dataReader.GetInt32(2);
                    region.Id_secteur = dataReader.GetInt32(3);

                    listeDeRegion.Add(region);
                }

                dataReader.Close();
                Api.Instance.tempFile.arrayRegion = listeDeRegion;
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();
        }


        public void getFrais()
        {
            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            List<Visite> tempVisite = Api.Instance.tempFile.arrayVisite;
            List<FraisDeRoute> listesFrais = new List<FraisDeRoute>();
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `frais`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {
                    FraisDeRoute unFrais = new FraisDeRoute();
                    unFrais.Id = dataReader.GetInt32(0);
                    unFrais.Type = dataReader.GetString(1);
                    unFrais.Tarif = dataReader.GetFloat(2);
                    unFrais.Status = dataReader.GetInt32(3);
                    unFrais.Fk_visite_id = dataReader.GetInt32(4);
                    listesFrais.Add(unFrais);

                    double frais = dataReader.GetDouble(2);
                    int fk_Visite = dataReader.GetInt32(4);

                    for (int i = 0; i < tempVisite.Count; i++)
                    {
                        if (fk_Visite == tempVisite[i].Id)
                        {
                            tempVisite[i].FraisDuJour += frais;
                        }
                    }

                    Api.Instance.tempFile.arrayFrais = listesFrais;
                    Api.Instance.tempFile.arrayVisite = tempVisite;
                }
                dataReader.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();
        }

        public void GetNotes()
        {

            MySqlConnection maConnexion = new MySqlConnection(maChaineconnexion);
            List<Notes> arrayNotes = new List<Notes>();
            try
            {
                maConnexion.Open();
                maCommande.Connection = maConnexion;
                maCommande.CommandType = CommandType.Text;
                maCommande.CommandText = maCommande.CommandText = "SELECT * FROM `note`";
                MySqlDataReader dataReader = maCommande.ExecuteReader();

                while (dataReader.Read())
                {
                    Notes note = new Notes();
                    note.Id = dataReader.GetInt32(0);
                    note.Sujet = dataReader.GetString(1);
                    note.Message = dataReader.GetString(2);
                    arrayNotes.Add(note);
                }
                Api.Instance.tempFile.arrayNotes = arrayNotes;

                dataReader.Close();
            }
            catch (Exception)
            {
                MessageBox.Show("Il y a vraisemblablement un problème !", "La connexion a échoué");
            }

            maConnexion.Close();
        }
    }
}