﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPE_Rh_V2
{
    public class Notes
    {
        int id;
        string sujet, message;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public string Message
        {
            get
            {
                return message;
            }

            set
            {
                message = value;
            }
        }

        public string Sujet
        {
            get
            {
                return sujet;
            }

            set
            {
                sujet = value;
            }
        }

        public Notes()
        {

        }

        public Notes(string sujet, string message)
        {
            this.sujet = sujet;
            this.message = message;
        }
    }
}
