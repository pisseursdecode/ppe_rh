﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{
    public partial class Planning : MetroForm
    {
        public Planning()
        {
            InitializeComponent();
        }

        private void Planning_Load(object sender, EventArgs e)
        {
            loadData();

            /// c'est quoi ce truc ? 
 
            foreach (var personne in Api.Instance.tempFile.arrayPersonnes)
            {
                if (personne.Type == ConsultationPersonne.prescripteurs)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = personne.Nom + " " + personne.Prenom;
                    item.Value = personne.Id;

                    metroComboBox1.Items.Add(item);
                }
            }
        }


        public void loadData()
        {
            dataGridView1.Rows.Clear();
            Api.Instance.interrogationServer.getPersonne();
            Api.Instance.interrogationServer.getVisites();
            Api.Instance.interrogationServer.getFrais();

            metroTile1.Text = "0";
            metroTile2.Text = "0";

            foreach (Visite item in Api.Instance.tempFile.arrayVisite)
            {

                dataGridView1.Rows.Add(item.Id, (item.DebutVisite).Replace("/", "-"), item.Nom, item.Prenom, item.Adresse, item.Complemment, item.CodePostal, item.Ville, item.Telephone, item.Email, (item.FinVisite).Replace("/", "-"));

                if (checkDataTime(item.DebutVisite, 0) == true)
                {
                    metroTile1.Text = Convert.ToString(Convert.ToInt32(metroTile1.Text) + Convert.ToInt32(item.FraisDuJour));
                }

                if (checkDataTime(item.DebutVisite, 1) == true)
                {
                    metroTile2.Text = Convert.ToString(Convert.ToInt32(metroTile2.Text) + Convert.ToInt32(item.FraisDuJour));
                }
            }

            foreach (Personne item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Login == Api.Instance.tempFile.idUser && item.Mdp == Api.Instance.tempFile.passwordUser)
                {
                    metroTextBox1.Text = "Nom : " + item.Nom;
                    metroTextBox2.Text = "Prenom : " + item.Prenom;

                }
            }

            metroTile1.Text += " €";
            metroTile2.Text += " €";

            dataGridView1.AllowUserToAddRows = false;
        }

        private void metroDateTime1_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime1.Format = DateTimePickerFormat.Time;
            this.metroDateTime1.Width = 100;
            this.metroDateTime1.ShowUpDown = true;
        }

        private void metroDateTime2_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime2.Format = DateTimePickerFormat.Time;
            this.metroDateTime2.Width = 100;
            this.metroDateTime2.ShowUpDown = true;
        }

        private bool checkDataTime(string dateDeLaVisite, int tagDataTime)
        {
            bool statusDate = false;
            var currentDate = Convert.ToString(DateTime.Today);
            string dateParse = "";
            string dateDeLaVisiteParse = "";

            if (tagDataTime == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    dateParse += currentDate[i];
                    dateDeLaVisiteParse += dateDeLaVisite[i];
                }
                // MessageBox.Show(dateParse + "------" + dateDeLaVisiteParse);
                if (dateParse == dateDeLaVisiteParse)
                {
                    statusDate = true;
                }
            }
            else
            {
                for (int i = 3; i < 10; i++)
                {
                    dateParse += currentDate[i];
                    dateDeLaVisiteParse += dateDeLaVisite[i];
                }

                if (dateParse == dateDeLaVisiteParse)
                {
                    statusDate = true;
                }
            }


            return statusDate;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            if (metroComboBox1.Text != "")
            {
                Ajout_visite Visite = new Ajout_visite(this);
                Visite.Show();
            }
            else {
                MessageBox.Show("Veuillez sélectionner un prescripteur afin d'ajouter une visite, merci.");
            }
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow row in this.dataGridView1.Rows)
            {
                Visite visite = new Visite();
                visite.Id = (Int32)row.Cells[0].Value;
                visite.DebutVisite = (String)row.Cells[1].Value;
                visite.Nom = (String)row.Cells[2].Value;
                visite.Prenom = (String)row.Cells[3].Value;
                visite.Adresse = (String)row.Cells[4].Value;
                visite.Complemment = (String)row.Cells[5].Value;
                visite.CodePostal = (Int32)row.Cells[6].Value;
                visite.Ville = (String)row.Cells[7].Value;
                visite.Telephone = (Int32)row.Cells[8].Value;
                visite.Email = (String)row.Cells[9].Value;
                visite.FinVisite = (String)row.Cells[10].Value;
                Api.Instance.interrogationServer.gestionVisites(ActionRequete.modifier, visite);
            }

            loadData();
            MessageBox.Show("Les modifications ont bien été apporté ", "Message");
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count == 0 && dataGridView1.SelectedRows.Count == 0)
            {
                MessageBox.Show("Aucune ligne sélectionnée", "Message");
            }
            else
            {
                foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
                {
                    Visite visite = new Visite();
                    visite.Id = (Int32)row.Cells[0].Value;
                    Api.Instance.interrogationServer.gestionVisites(ActionRequete.supprimer, visite);
                }

                loadData();
                MessageBox.Show("Les modifications ont bien été apporté ", "Message");
            }
        }
    }
}
