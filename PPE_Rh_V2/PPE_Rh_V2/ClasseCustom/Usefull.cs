﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PPE_Rh_V2
{
    // Action possible sur la base de donnée
    public class ActionRequete
    {
        public static string ajouter = "Ajouter";
        public static string modifier = "Modifier";
        public static string supprimer = "Supprimer";
    }

    // liste des personnes 
    public class ConsultationPersonne
    {
        public static int visiteurs = 0;
        public static int prescripteurs = 1;
        public static int responsables = 2;
        public static int deleges = 3;
        public static int tout = 4; // tout
    }

    // liste des tables de base de donnée
    public class ActionSurLaTable
    {
        public static string Collaborateur = "Collaborateur";
        public static string Visites = "Visites";
        public static string FraisDeRoute = "FraisDeRoute";
        public static string RetourInformationDesVisites = "DeRetourInformationDesVisiteslégués";
        public static string DeToutTypeDeFrais = "DeToutTypeDeFrais";
        public static string Geographique = "Géographiquégués";
    }

    public class MethodeUtile
    {
        public void quitter()
        {
            var resultat = MessageBox.Show("Voulez vous vraiment quitter ?", "Quitter", MessageBoxButtons.OKCancel);
            if (resultat == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        public  string mdpAleatoir(int length)
        {

            var chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            var stringChars = new char[length];
            var random = new Random();

            for (int i = 0; i < stringChars.Length; i++)
            {
                stringChars[i] = chars[random.Next(chars.Length)];
            }

            var finalString = new String(stringChars);

            return finalString;
        }
    }


    public class ChangerLeTitre
    {
        public void enregistrerLeTitre(string titre, int tag)
        {
            Api.Instance.tempFile.titreAjoutPersonne = titre;
            Api.Instance.tempFile.tagAjout = tag;
        }

        public string recupererLeTitre()
        {
            return Api.Instance.tempFile.titreAjoutPersonne;
        }
    }
}
