﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{
    public partial class Planning_visiteur : MetroForm
    {
        public Planning_visiteur()
        {
            InitializeComponent();
        }

        private void Planning_visiteur_Load(object sender, EventArgs e)
        {
            Api.Instance.interrogationServer.getVisites();

            foreach (Visite item in Api.Instance.tempFile.arrayVisite)
            {
                dataGridView1.Rows.Add(item.Fk_id_note, item.DebutVisite, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.CodePostal, item.Ville, item.Telephone, item.Email, item.FinVisite, item.Id);
            }

            foreach (Personne item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Login == Api.Instance.tempFile.idUser && item.Mdp == Api.Instance.tempFile.passwordUser)
                {
                    metroTextBox1.Text = "Nom : " + item.Nom;
                    metroTextBox2.Text = "Prenom : " + item.Prenom;
                }
            }
        }
        private void metroDateTime1_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime1.Format = DateTimePickerFormat.Time;
            this.metroDateTime1.Width = 100;
            this.metroDateTime1.ShowUpDown = true;
        }

        private void metroDateTime2_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime2.Format = DateTimePickerFormat.Time;
            this.metroDateTime2.Width = 100;
            this.metroDateTime2.ShowUpDown = true;
        }

        private void metroButton1_Click(object sender, EventArgs e)
        {
            Ajout_Note ajou = new Ajout_Note();
            ajou.Show();
        }

        private void metroButton4_Click(object sender, EventArgs e)
        {
            Ajouter_coût ajouter = new Ajouter_coût();
            ajouter.Show();
        }

        private void metroButton2_Click(object sender, EventArgs e)
        {// modif notes 
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                //Ajout_Note modifier = new Ajout_Note((Int32)row.Cells[0].Value);
                //modifier.Show();
            }

        }

        private void metroButton5_Click(object sender, EventArgs e)
        {// modif frais
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                //Ajouter_coût modifier = new Ajouter_coût((Int32)row.Cells[11].Value);
                //modifier.Show();
            }
            loadData();
        }

        private void metroButton3_Click(object sender, EventArgs e)
        {//Suppression notes
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                Notes note = new Notes();
                note.Id = (Int32)row.Cells[0].Value;
                Api.Instance.interrogationServer.gestionDesNote(ActionRequete.supprimer, note);
            }
            MessageBox.Show("Les modifications ont bien été apporté ");
        }

        private void metroButton6_Click(object sender, EventArgs e)
        {//Suppression frais
            foreach (DataGridViewRow row in this.dataGridView1.SelectedRows)
            {
                FraisDeRoute frais = new FraisDeRoute();
                frais.Fk_visite_id = (Int32)row.Cells[11].Value;
                Api.Instance.interrogationServer.gestionFraisDeRoute(ActionRequete.supprimer, frais);
            }
            MessageBox.Show("Les modifications ont bien été apporté ");
        }
        public void loadData()
        {
            dataGridView1.Rows.Clear();
            Api.Instance.interrogationServer.getVisites();
            Api.Instance.interrogationServer.getFrais();

            metroTextButton1.Text = "0";
            metroTextButton2.Text = "0";

            foreach (Visite item in Api.Instance.tempFile.arrayVisite)
            {
                dataGridView1.Rows.Add(item.Id, item.DebutVisite, item.Nom, item.Prenom, item.Adresse, item.Complemment, item.CodePostal, item.Ville, item.Telephone, item.Email, item.FinVisite);

                if (checkDataTime(item.DebutVisite, 0) == true)
                {
                    metroTextButton1.Text = Convert.ToString(Convert.ToInt32(metroTextButton1.Text) + Convert.ToInt32(item.FraisDuJour));
                }

                if (checkDataTime(item.DebutVisite, 1) == true)
                {
                    metroTextButton2.Text = Convert.ToString(Convert.ToInt32(metroTextButton2.Text) + Convert.ToInt32(item.FraisDuJour));
                }
            }

            foreach (Personne item in Api.Instance.tempFile.arrayPersonnes)
            {
                if (item.Login == Api.Instance.tempFile.idUser && item.Mdp == Api.Instance.tempFile.passwordUser)
                {
                    metroTextBox1.Text = "Nom : " + item.Nom;
                    metroTextBox2.Text = "Prenom : " + item.Prenom;

                }
            }

            metroTextButton1.Text += " €";
            metroTextButton2.Text += " €";

            dataGridView1.AllowUserToAddRows = false;
        }
        private bool checkDataTime(string dateDeLaVisite, int tagDataTime)
        {
            bool statusDate = false;
            var currentDate = Convert.ToString(DateTime.Today);
            string dateParse = "";
            string dateDeLaVisiteParse = "";

            if (tagDataTime == 0)
            {
                for (int i = 0; i < 10; i++)
                {
                    dateParse += currentDate[i];
                    dateDeLaVisiteParse += dateDeLaVisite[i];
                }
                // MessageBox.Show(dateParse + "------" + dateDeLaVisiteParse);
                if (dateParse == dateDeLaVisiteParse)
                {
                    statusDate = true;
                }
            }
            else
            {
                for (int i = 3; i < 10; i++)
                {
                    dateParse += currentDate[i];
                    dateDeLaVisiteParse += dateDeLaVisite[i];
                }

                if (dateParse == dateDeLaVisiteParse)
                {
                    statusDate = true;
                }
            }
            return statusDate;
        }
    }
}
