﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;

namespace PPE_Rh_V2
{
    public partial class Notes_des_visites : MetroForm
    {
        public Notes_des_visites()
        {
            InitializeComponent();
        }

        private void metroDateTime1_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime1.Format = DateTimePickerFormat.Time;
            this.metroDateTime1.Width = 100;
            this.metroDateTime1.ShowUpDown = true;
        }

        private void metroDateTime2_ValueChanged(object sender, EventArgs e)
        {
            this.metroDateTime2.Format = DateTimePickerFormat.Time;
            this.metroDateTime2.Width = 100;
            this.metroDateTime2.ShowUpDown = true;
        }


    }
}
