﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPE_Rh_V2
{
    public class Region
    {
        int id;
        string nom;
        int id_secteur;
        int id_responsable;

        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }



        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public int Id_secteur
        {
            get
            {
                return id_secteur;
            }

            set
            {
                id_secteur = value;
            }
        }

        public int Id_responsable
        {
            get
            {
                return id_responsable;
            }

            set
            {
                id_responsable = value;
            }
        }

        public Region(string nom, int id_secteur, int id_responsable)
        {
            this.nom = nom;
            this.Id_secteur = id_secteur;
            this.Id_responsable = id_responsable;
        }

        public Region()
        {

        }


    }
}
