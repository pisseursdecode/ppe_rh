﻿
namespace PPE_Rh_V2
{
    public class Visite
    {
        int id;
        string nom;
        string prenom;
        string adresse;
        string complemment;
        string ville;
        int codePostal;
        int telephone;
        string email;
        string debutVisite;
        string finVisite;
        int fk_id_note;
        int fk_id_collaborateur;
        string prescripteur;
        double fraisDuJour;
        

        public string Nom
        {
            get
            {
                return nom;
            }

            set
            {
                nom = value;
            }
        }

        public string Prenom
        {
            get
            {
                return prenom;
            }

            set
            {
                prenom = value;
            }
        }

        public string Adresse
        {
            get
            {
                return adresse;
            }

            set
            {
                adresse = value;
            }
        }

        public string Complemment
        {
            get
            {
                return complemment;
            }

            set
            {
                complemment = value;
            }
        }

        public string Ville
        {
            get
            {
                return ville;
            }

            set
            {
                ville = value;
            }
        }

        public string Prescripteur
        {
            get
            {
                return prescripteur;
            }

            set
            {
                prescripteur = value;
            }
        }

        public int CodePostal
        {
            get
            {
                return codePostal;
            }

            set
            {
                codePostal = value;
            }
        }

        public int Telephone
        {
            get
            {
                return telephone;
            }

            set
            {
                telephone = value;
            }
        }

        public string Email
        {
            get
            {
                return email;
            }

            set
            {
                email = value;
            }
        }

        public string DebutVisite
        {
            get
            {
                return debutVisite;
            }

            set
            {
                debutVisite = value;
            }
        }

        public string FinVisite
        {
            get
            {
                return finVisite;
            }

            set
            {
                finVisite = value;
            }
        }

public int Fk_id_note
        {
            get
            {
                return fk_id_note;
            }

            set
            {
                fk_id_note = value;
            }
        }

        public int Fk_id_collaborateur
        {
            get
            {
                return fk_id_collaborateur;
            }

            set
            {
                fk_id_collaborateur = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

         public double FraisDuJour
        {
            get
            {
                return fraisDuJour;
            }

            set
            {
                fraisDuJour = value;
            }
        }

        public Visite(string nom, string prenom, string adresse, string complemment, string ville, int codePostal, int telephone, string email, string debutVisite, string finVisite,string prescripteur, double fraisDuJour)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.adresse = adresse;
            this.complemment = complemment;
            this.ville = ville;
            this.codePostal = codePostal;
            this.telephone = telephone;
            this.email = email;
            this.debutVisite = debutVisite;
            this.finVisite = finVisite;
            this.prescripteur  = prescripteur;
            this.fraisDuJour = fraisDuJour;
        }

        public Visite(string debutVisite, string nom, string prenom, string adresse, string complemment, int codePostal, string ville, int telephone, string email, string finVisite)
        {
            this.debutVisite = debutVisite;
            this.nom = nom;
            this.prenom = prenom;
            this.adresse = adresse;
            this.complemment = complemment;
            this.codePostal = codePostal;
            this.ville = ville;
            this.telephone = telephone;
            this.email = email;
            this.finVisite = finVisite;
        }

        public Visite(){
        }
    }
}
