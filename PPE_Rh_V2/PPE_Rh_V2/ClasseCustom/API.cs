﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPE_Rh_V2
{
    public class Api
    {
        private static Api instance = null;
        public MethodeUtile methodeUtile = new MethodeUtile();
        public InterrogationServer interrogationServer = new InterrogationServer();
        public ChangerLeTitre changerLeTitre = new ChangerLeTitre();
        public TempFile tempFile = new TempFile();

        private Api()
        {
        }

        public static Api Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Api();
                }
                return instance;
            }
        }
    }
}