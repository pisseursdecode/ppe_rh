﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MetroFramework.Forms;
using PPE_Rh_V2;

namespace PPE_Rh_V2
{
    public partial class Ajout_Secteur : MetroForm
    {

        CentreDeControle centre = null;

        public Ajout_Secteur(CentreDeControle centrePost)
        {
            InitializeComponent();
            centre = centrePost;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Secteur unSecteur = new Secteur(metroTextBox1.Text, (metroComboBox1.SelectedItem as ComboboxItem).ToValue());
            Api.Instance.interrogationServer.gestionSecteurs(ActionRequete.ajouter, unSecteur);
           // centre.loadData();
            this.Close();
        }

        private void Ajout_Secteur_Load(object sender, EventArgs e)
        {
            foreach (var personne in Api.Instance.tempFile.arrayPersonnes)
            {
                if (personne.Type == ConsultationPersonne.responsables)
                {
                    ComboboxItem comboItem = new ComboboxItem();
                    comboItem.Text = personne.Nom + " " + personne.Prenom;
                    comboItem.Value = personne.Id;

                    metroComboBox1.Items.Add(comboItem);
                }
            }
        }
    }
}
